function alert() {
    return `
    <div class="alert alert-danger" role="alert">
        There is an error!
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = `http://localhost:8000/api/locations/`;

    try {
        const response = await fetch(locationUrl);

        if (!response.ok) {
            const html = alert();
            const column = column[1];
            column.innerHTML += html;
        } else {
          const data = await response.json();
          const selectTag = document.getElementById("location");

          for (let location of data.locations) {
              const option = document.createElement("option");
              option.value = location.id;
              option.innerHTML = location.name;
              selectTag.appendChild(option);

          }
        }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (conferenceResponse.ok) {
            formTag.reset();
            const newConference = await conferenceResponse.json();

        }
    });
}
    catch (e) {
        console.log(e);
    }
});
