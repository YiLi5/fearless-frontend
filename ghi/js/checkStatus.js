const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
  const encodedPayload = payloadCookie.value;
  const decodedPayload = atob(encodedPayload);
  const payload = JSON.parse(decodedPayload);

  if(payload.user.perms.includes("events.add_conference")){
    const conferenceTag = document.getElementById("conferenceLink");
    conferenceTag.classList.remove("d-none");
  }

  if(payload.user.perms.includes("events.add_location")){
    const locationTag = document.getElementById("locationLink");
    locationTag.classList.remove("d-none");
  }
  
}
