function alert() {
    return `
    <div class="alert alert-danger" role="alert">
        There is an error!
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const conferenceUrl = `http://localhost:8000/api/conferences/`;
    const selectTag = document.getElementById("conference");

    try {
        const response = await fetch(conferenceUrl);

        if (!response.ok) {
            const html = alert();
            const column = column[1];
            column.innerHTML += html;
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
              const option = document.createElement("option");
              option.value = conference.id;
              option.innerHTML = conference.name;
              selectTag.appendChild(option);

          }
        }


    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceId = selectTag.options[selectTag.selectedIndex].value;
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchConfig);
        if (presentationResponse.ok) {
            formTag.reset();
            const newPresentation = await presentationResponse.json();

        }
    });
}
    catch (e) {
        console.log(e);
    }
});
