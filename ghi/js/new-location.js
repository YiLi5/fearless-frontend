
function alert() {
    return `
    <div class="alert alert-danger" role="alert">
        There is an error!
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = `http://localhost:8000/api/states/`;

    try {
        const response = await fetch(stateUrl);

        if (!response.ok) {
            const html = alert();
            const column = column[1];
            column.innerHTML += html;
        } else {
          const data = await response.json();
          const selectTag = document.getElementById("state");

          for (let state of data.states) {
              const option = document.createElement("option");
              option.value = state.abbreviation;
              option.innerHTML = state.name;
              selectTag.appendChild(option);

          }
        }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            formTag.reset();
            const newLocation = await locationResponse.json();

        }
    });
}
    catch (e) {
        console.log(e);
    }
});
